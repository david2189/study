
$(document).ready(function(){
    $("table tbody").on("change", "input[type=checkbox]", function (e) {		
        
        var currentCB = $(this);
        var grid = $('#grid');
        var isChecked = this.checked;
        if (currentCB.is(".groupHeader")) {	//if group header is checked, to check all child checkboxes						
            var checkboxes = currentCB.closest('tr').
              nextUntil('tr.gridghead_0').find('.cbox[type="checkbox"]');
            checkboxes.each(function(){
                if (!this.checked || !isChecked)                   
                    grid.setSelection($(this).closest('tr').attr('id'), true); 
            });			
        } else {  //when child checkbox is checked
                var allCbs = currentCB.closest('tr').prevAll("tr.gridghead_0:first").
                                nextUntil('tr.gridghead_0').andSelf().find('[type="checkbox"]');
                var allSlaves = allCbs.filter('.cbox');
                var master = allCbs.filter(".groupHeader");
                                                    
                var allChecked = !isChecked ? false : allSlaves.filter(":checked").length === allSlaves.length;
                master.prop("checked", allChecked);
            }
        });	
  
    

    
    $("#grid").jqGrid({ 
        data: mydata, 
        datatype: "local", 
        height: 'auto',     
        colNames:[
            'Inv No',
            'Date', 
            'Client', 
            'Amount',
            'Tax',
            'Total',
            'Notes'
        ], 
        colModel:[ 
            {
                name:'id',index:'id', 
                width:60, sorttype:"int"
            }, 
            {
                name:'invdate',index:'invdate', 
                width:90, sorttype:"date", formatter:"date"
            }, 
            {
                name:'name',index:'name', 
                width:100, editable:true
            }, 
            {
                name:'amount',index:'amount', 
                width:80, align:"right",
                sorttype:"float", formatter:"number",
                editable:true
            }, 
            {
                name:'tax',index:'tax', 
                width:80, align:"right",
                sorttype:"float", editable:true
            },
            {
                name:'total',index:'total',
                width:80,align:"right",sorttype:"float"
            },
            {
                name:'note',index:'note', 
                width:150, sortable:false
            } 
        ],     
        viewrecords: true, 
        sortname: 'name', 
        pager: "#jqGridPager",
        multiselect: true,
        grouping:true,    
        groupingView : { 
            groupField : ['name'],
            groupText : ['<input type="checkbox" class="groupHeader"/> <b>  {0}  </b>'],
            groupColumnShow : [false],
        }, 
        //to check header checkboxes when 'select all' checkbox is checked
        onSelectAll: function(rowIds, allChecked) {
                $("input.groupHeader").attr('checked', allChecked);
        }        
    });  
    
    ChangejQGridDesign("#grid", "#jqGridPager");
});