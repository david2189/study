$(document).ready(function(){
    //define some sample data

   
    $( "#btnPrueba" ).click(function() {
        var a =$("#example-table").tabulator("getData");
        //console.log(a[0].row.data);
        console.log(a);
         });

//create Tabulator on DOM element with id "example-table"
$("#example-table").tabulator({
    height:205, // set height of table (in CSS or here), this enables the Virtual DOM and improves render speed dramatically (can be any valid css height value)
    layout:"fitColumns", //fit columns to width of table (optional)
    columns:[ //Define Table Columns
        {title:"Name", field:"name", width:150},
        {title:"Age", field:"age", align:"left", formatter:"progress", width:150},        
        {title:"Date Of Birth", field:"dob", sorter:"date", align:"center", width:150},
        {title:"Acciones", field:"col", width:200, formatter:function(cell, formatterParams){
            var value = cell.getValue();
            var row=cell.getData();
        //    console.log(row.id);
             if(value==1){
                 return "<input type='radio' name='rbtn"+row.id+"' value='1' checked='checked' >GRAL"
                        +"<input type='radio'name='rbtn"+row.id+"' value='2'>SUB"
                        +"<input type='radio'name='rbtn"+row.id+"' value='3'>NONE"

             }else if(value==2){
                return "<input type='radio' name='rbtn"+row.id+"' value='1' >GRAL"
                +"<input type='radio' name='rbtn"+row.id+"' value='2'  checked='checked'>SUB"
                +"<input type='radio' name='rbtn"+row.id+"' value='3'>NONE"
             }else{
                return "<input type='radio' name='rbtn"+row.id+"' value='1' >GRAL"
                +"<input type='radio' name='rbtn"+row.id+"' value='2'  >SUB"
                +"<input type='radio' name='rbtn"+row.id+"' value='3' checked='checked'>NONE"
             }
         }},
    ]   
});
var tabledata = [
    {id:1, name:"Oli Bob", age:"12", col:"1", dob:""},
    {id:2, name:"Mary May", age:"1", col:"2", dob:"14/05/1982"},
    {id:3, name:"Christine Lobowski", age:"42", col:"1", dob:"22/05/1982"},
    {id:4, name:"Brendon Philips", age:"125", col:"3", dob:"01/08/1980"},
    {id:5, name:"Margret Marmajuke", age:"16", col:"2", dob:"31/01/1999"},
];

//load sample data into the table
$("#example-table").tabulator("setData", tabledata);

});

