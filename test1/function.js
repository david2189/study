$(document).ready(function () {
    $("#jqGrid").jqGrid({
        url: 'data.json',
        mtype: "GET",
        datatype: "json",
        colModel: [
            { label: 'OrderID', name: 'OrderID', key: true, width: 75 },
             { label: 'Customer ID', name: 'CustomerID', width: 150 },
            { label: 'Order Date', name: 'OrderDate', width: 150 },
            { label: 'Freight', name: 'Freight', width: 150 },
            { label: 'Employee ID', name: 'EmployeeID', width: 150 }
        ],
        loadonce:true,
        viewrecords: true,
        width: 780,
        height: 250,
        rowNum: 20,
        rowList: [20,30,50],
         multiselect: true,
        sortname: 'OrderDate',
        pager: "#jqGridPager",
        grouping: true,
        groupingView: {
            groupField: ["CustomerID"],
            groupColumnShow: [true, true],         
            groupText: ['<input type="checkbox" class="groupHeader"/>  CustomerID: <b>{0}</b>'],
            groupOrder: ["asc"],
            groupSummary: [false, false],
            groupCollapse: false
        },
         onSelectAll: function(rowIds, allChecked) {
             $("input.groupHeader").attr('checked', allChecked);           
    },
    });
/*
    $("table tbody").on("change", "input[type=checkbox]", function (e) {		
    
        var currentCB = $(this);
        console.log(currentCB);
        var grid = $('#jqGrid');
        var isChecked = this.checked;
        if (currentCB.is(".groupHeader")) {	//if group header is checked, to check all child checkboxes						
            var checkboxes = currentCB.closest('tr').
              nextUntil('tr.gridghead_0').find('.cbox[type="checkbox"]');
            checkboxes.each(function(){
                if (!this.checked || !isChecked)                   
                    grid.setSelection($(this).closest('tr').attr('id'), true); 
            });			
        } else {  //when child checkbox is checked
                var allCbs = currentCB.closest('tr').prevAll("tr.gridghead_0:first").
                                nextUntil('tr.gridghead_0').andSelf().find('[type="checkbox"]');
                var allSlaves = allCbs.filter('.cbox');
                var master = allCbs.filter(".groupHeader");
                                                    
                var allChecked = !isChecked ? false : allSlaves.filter(":checked").length === allSlaves.length;
                master.prop("checked", allChecked);
            }});*/
});